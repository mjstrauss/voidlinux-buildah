#!/usr/bin/env bash
set -euxo pipefail
CONTAINER=$(buildah from scratch)
MOUNTPOINT=$(buildah mount $CONTAINER)

# Install Void repo keys
buildah copy $CONTAINER '/var/db/xbps/keys/*' '/var/db/xbps/keys/'

# Bootstrap Void
ARCH="x86_64-musl"
REPO="https://repo-de.voidlinux.org/current/musl"
XBPS_ARCH=$ARCH xbps-install -Sy -R $REPO -r $MOUNTPOINT base-minimal bash buildah

# Clean cache
xbps-remove -O -r $MOUNTPOINT

buildah unmount $CONTAINER
buildah commit --squash $CONTAINER voidlinux-buildah
buildah tag voidlinux-buildah registry.gitlab.com/mjstrauss/voidlinux-buildah
